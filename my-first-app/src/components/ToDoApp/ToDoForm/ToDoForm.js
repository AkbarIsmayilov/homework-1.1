import React from 'react';

const ToDoForm = ({addToDoHandler}) => {
    let input ; 
    
    return (
        <div>
            <input type="text" ref={ node => {
                input = node
            }} />

            <button onClick={ () => { addToDoHandler (input.value);
                        input.value = '';
                    }} > + </button>

        </div>
    );
};

export default ToDoForm;