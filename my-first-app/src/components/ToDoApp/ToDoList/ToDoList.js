import React from 'react';
import Todo from './ToDo/Todo';

const ToDoList = ({toDos,remove }) => {
 
    const todolist = toDos.map( (item ,ind) => <Todo key= {ind  }todo={item} remove= {remove}/> );
    return (
                <ul>  {todolist}</ul>  
      
    );
};

export default ToDoList;