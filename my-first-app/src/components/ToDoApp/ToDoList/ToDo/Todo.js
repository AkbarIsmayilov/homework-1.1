import React from 'react';

const Todo = ({remove, todo}) => {
    return (
    <li onClick={remove}>{todo.text}</li>
    );
};

export default Todo;