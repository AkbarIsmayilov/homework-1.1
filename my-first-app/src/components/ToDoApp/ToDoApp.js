import React, { Component } from 'react';
import ToDoForm from './ToDoForm/ToDoForm';
import './ToDoApp.scss';
import ToDoList from './ToDoList/ToDoList';
import Title from './Title/Title';


class ToDoApp extends Component {
    constructor(props) {
        super(props);
        
    this.state = {
        data: []
    }
    }
    
    addToDo (val) {
       let  newData = this.state.data.push(val) ;
        this.setState({
            data: newData
        });



    }

    componentDidMount = {}
    render() {
        return (
            <div>
                <Title text={"TO _ DO App"}/>

                <ToDoForm addToDoHandler = {this.addToDo = this.addToDo.bind(this)} />
                <ToDoList toDos = {this.state.data}/>
            </div>
        );
    }
}

export default ToDoApp;