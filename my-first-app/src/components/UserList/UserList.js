import React, {Component} from 'react';
// import axios from 'axios';
import User from "./User/User";


class UserList extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        persons : []
    }


    componentDidMount() {
        // fetch(this.props.url)
        //     .then(res => { res.json()})
        //     .then(data => {this.setState({persons: data })})

        fetch(this.props.url).then(r => r.json())
          .then(data => {
              console.log(data);
            this.setState({
              users: data
            })
          });


        // axios.get(this.props.url)
        //     .then(res => {this.setState( {persons : res.data})})
    }

    render() {
        return (
           <>
               {this.state.persons.map(user => <User key={user.id} info={user} />)}
           </>
        );
    }
}

export default UserList;