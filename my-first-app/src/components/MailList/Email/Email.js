import  React, {useState} from 'react';


const Email = (props) => {
    const {subject, mailFrom ,mailTo, text } = props.self;
    const [isTextVisible,setTextVisible] = useState({isTextVisible:false});
    const toggleText = () =>  {
        setTextVisible(!isTextVisible);
    } ;

    return (
        <div onClick={toggleText}>
            <h1 className="email-subject">{subject}</h1>
            <p className="email-from"> from: {mailFrom}</p>
            <p className="email-to">to: {mailTo}</p>
            <p hidden={isTextVisible} className="email-text">{text}</p>
        </div>
    );
};

export default Email;