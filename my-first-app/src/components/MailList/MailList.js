import React , {useState} from 'react';
import Email from './Email/Email';
import './Email/Email.scss';
import './MailList.scss'

const MailList = (props) => {

    const [textToggle, setToggle] = useState({textToggle: false});
    const toggleFolder  = () => {
        setToggle(!textToggle)
    };

    const emails = props.emails.map(( item, ind ) => <Email key={ind} self={item}/> );
    emails.reverse();
    return (
          <div  className="folder-wrapper" onClick={toggleFolder}>
              <h2 className="folder-header">{props.folderName}</h2>
              <div hidden={textToggle} className="folder-email">{emails}</div>
          </div>

    );
};

export default MailList;