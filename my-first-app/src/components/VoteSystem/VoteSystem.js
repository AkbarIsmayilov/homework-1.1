import React, {Component} from 'react';
import VoteItem from './VoteItem/VoteItem';
import PropTypes from 'prop-types';


class VoteSystem extends Component {

    constructor(props) {
        super(props);
        let updatedVoteItem = props.items.map(vI => ({name: vI, votes: 0}));
        this.state = {
            votes: [...updatedVoteItem]
        }
        console.log("constructor");
        // this.voteClickHandler().bind(this);
    }

    title = React.createRef();
    info =React.createRef();




    voteClickHandler = (e) => {
        let {votes} = this.state;
        const clickedItem = votes.find(vI => (vI.name === e.currentTarget.firstChild.textContent))
        clickedItem.votes++;
        this.setState({votes: [...votes]});
        e.currentTarget.style.backgroundColor = 'blue';
        this.title.current.style.color = 'red';
        this.InformUser(e);

        setTimeout(() => {
            // this.e.currentTarget.children[1].style.backgroundColor = 'yellow';
            this.title.current.style.color = null;
        }, 2000)

        e.currentTarget.style.backgroundColor = null;


    }

    // static defaultProps  =  {
    //     items : ["33" , "44 " ," 55"]
    // }

    InformUser= ( e) =>   {
        this.info.current.innerHTML = "You pressed " + e.currentTarget.firstChild.textContent  + " item " ;

    setTimeout(() => {this.info.current.innerHTML = ''}, 5000);

};

    componentDidMount() {
        console.log("mounted");
    }

    render() {

        let {votes} = this.state;
        let voteItem = votes.map((vI, ind) => (
            <VoteItem id={ind} name={vI.name} votes={vI.votes} voteHandler={this.voteClickHandler}/>));

    console.log("rendering");
        return (
            <div className="vote-system-container">
                <h1 className="vote-heading" ref={this.title}>Vote for BETTER Future </h1>
                <div className="">{voteItem}</div>

                <p className="inform-text" ref={this.info}></p>
            </div>

        )

    }
}
// VoteSystem.defaultProps = {
//     items: [ "123  ", "234  ", "456   "]
// }

//
VoteSystem.propTypes = {
    items: PropTypes.arrayOf(PropTypes.string),
    number : PropTypes.number.isRequired
}
export default VoteSystem;