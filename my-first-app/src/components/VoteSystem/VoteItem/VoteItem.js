import React from 'react';

const VoteItem = (props) => {
    return (

        <div onClick={props.voteHandler}>
            <span>{props.name}</span>
            <span>{ props.votes }</span>
        </div>
    );
};

export default VoteItem;