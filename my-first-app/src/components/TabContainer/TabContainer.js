import React, {Component} from 'react';
import TabContent from './TabContent/TabContent';
import TabHeader from './TabHeader/TabHeader';
import './TabContainer.scss';

class TabContainer extends Component {
    constructor(props) {
        super(props);
    this.state = {
        activeTabIndex: 4
    }
    }


    render() {


        let  tabsHeader = [],
        activeTabContent = <TabContent text={this.props.contentArray[this.state.activeTabIndex].content}/>;
        this.props.contentArray.forEach(
        (elem,ind) => {
            const clickHandler = () => {
                this.setState({activeTabIndex: ind});
            };


            tabsHeader.push(
            <TabHeader
                 title={elem.title}
                key={ind}
                click={clickHandler} />)}
        );

        return (
            <div className={"tab-container"}>
                <div className="tab-header">{tabsHeader}</div>
                <div className="active-tab-content">{activeTabContent}</div>
            </div>
        );
    }
}

export default TabContainer;