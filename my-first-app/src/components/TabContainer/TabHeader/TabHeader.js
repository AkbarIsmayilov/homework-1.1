import React from 'react';
import './TabHeader.scss';

const TabHeader = (props) => {
    return (
        <div onClick={props.click}  className={"tab-header"}>
            {props.title}
        </div>
    );
};

export default TabHeader;