import React from 'react';
import './TabContent.scss'

const TabContent = (props) => {
    return (
        <div>
            {props.text}
        </div>
    );
};

export default TabContent;