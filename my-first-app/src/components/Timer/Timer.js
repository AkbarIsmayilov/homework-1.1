import "./TimerStyle.css";
import "./TimerBtn/TimerBtn"


import React, {Component} from 'react';
import TimerBtn from "./TimerBtn/TimerBtn";

class Timer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 0,
            intervalID: null,
            lapArr: []
        }
        this.startTimer = this.startTimer.bind(this);
        this.stopTimer = this.stopTimer.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
        this.makeLap = this.makeLap.bind(this);

    }

    timer = React.createRef();
    lapsPanel = React.createRef();

    startTimer(e) {
        let {time} = this.state;
        let timerC = setInterval(() => {
            ++time;
            this.timer.current.innerText = time;
            this.setState({
                time: time ,
                intervalID: timerC,
                lapArr: [0 , 0,0,0]
            })

        }, 1000);
    };

    stopTimer(e) {
      clearInterval(this.state.intervalID  );
    };

    resetTimer(e) {
        clearInterval(this.state.intervalID);
        this.setState({
            time: 0,
            intervalID: null
        });
        this.timer.current.innerText = 0;
    };

    makeLap(e) {
       let {time} = this.state;
        // let arrr = [];

       // this.setState({
       //     lapArr: arrr.push(time)
       // })
        // let lapsArr =  this.state.lapArr.map(tItem => ( <p className={"highlight-text"}>{tItem}</p> ));
       this.lapsPanel.current.innerText = time;
        this.lapsPanel.current.classList.add("highlight-text");
       setTimeout( () => {
          this.lapsPanel.current.classList.remove("highlight-text");
           console.log("hereeeee");
       },2000);


    }

    render() {
        return (
            <div>
                <p className={"time-counter"} ref={this.timer}>wewewe</p>
                <TimerBtn name={"Start"} timeHandler={this.startTimer}/>
                <TimerBtn name={"Stop"} timeHandler={this.stopTimer}/>
                <TimerBtn name={"Reset"} timeHandler={this.resetTimer}/>
                <TimerBtn name={"Lap"} timeHandler={this.makeLap}/>
                <div ref={this.lapsPanel}></div>
            </div>

        );
    }
}

export default Timer;