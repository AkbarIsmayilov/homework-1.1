import React, { Component } from 'react';
// import ToDoApp from './components/ToDoApp/ToDoApp'
import {Route, BrowserRouter, Link, Switch} from 'react-router-dom'


class App extends Component {
  render() {
    return (
      <div>
        <h2>i'm learning react </h2>
        <p>i am a react developer</p>
        <Link to={'/user'}>User</Link>
        <Link to={'/'}>Home</Link>
        <Link to={'/login'}>Login</Link>
        <Link to={'/signin'}>Sign In </Link>

        <Switch>
          <Route exact={true} path={'/'} render={() => (
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At culpa debitis dignissimos ea eaque esse, expedita id magni maxime non, numquam perferendis qui repellat, sequi soluta tempore temporibus totam voluptate!</p>
              )}/>
          <Route path={'/user'}>
            <h1>i am USER page</h1>
          </Route>
          <Route path={'/signin'}>
            <h1>i am Sign In  page</h1>
          </Route>
          <Route path={'/login'}>
            <h1>i am Login page</h1>
          </Route>



        </Switch>
          </div>
    );
  }
}

export default App;


// import React from "react";
// import ReactDOM from "react-dom";
// import {
//     BrowserRouter as Router,
//     Switch,
//     useLocation
// } from "react-router-dom";
//
// function usePageViews() {
//     let location = useLocation();
//     React.useEffect(() => {
//         ga.send(["pageview", location.pathname]);
//     }, [location]);
// }
//
// function App() {
//     usePageViews();
//     return <Switch>...</Switch>;
// }
// export default App;
