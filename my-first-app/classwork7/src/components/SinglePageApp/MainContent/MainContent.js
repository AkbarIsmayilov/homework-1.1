import React from 'react';
import {Route} from "react-router";

const MainContent = (props) => {

    const routes = props.pageContents.map((pI ,ind) => {
        if (pI.toLowerCase() === 'home') {
            return <Route key={ind} exact path={'/'} render={ () => <h1>{pI}</h1> }/>}else{
                return <Route key={ind} path={`/${pI.toLowerCase()}`} render={() => <h1>{pI}</h1>}/>;
                }});

    return (
    <div>{routes}</div>
    );
};

export default MainContent;