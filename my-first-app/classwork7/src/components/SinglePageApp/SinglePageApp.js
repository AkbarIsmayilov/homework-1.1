import React from 'react';
import HeaderNav from "./HeaderNav/HeaderNav";
import Footer from "./Footer/Footer";
import MainContent from "./MainContent/MainContent";
const SinglePageApp = (props) => {
    return (
        <div>
            <HeaderNav pageHeaders={props.navigation} />
            <MainContent pageContents={props.navigation}/>
            <Footer/>
        </div>
    );
};

export default SinglePageApp;