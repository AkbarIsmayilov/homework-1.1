import React from 'react';
import {Link} from "react-router-dom";

const HeaderNav = (props) => {
    const imgLogo = 'https://logodix.com/logo/2003843.jpg';


    const links = props.pageHeaders.map((nI, ind) => {
       if (nI.toLowerCase() === 'home' ) {
           return <Link key={ind} to={'/'}>{nI.toLowerCase()}</Link>
       }else {
           return <Link key={ind} to={`/${nI.toLowerCase()}`}>{nI}</Link>
       }
    });


    return (

        <header>
            <img src={imgLogo} style={{width: '140px'}} alt="" />
            <nav>
                 {links}
            </nav>
        </header>
    );
};

export default HeaderNav;